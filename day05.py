import unittest

from utils import load_string, run_testCase


def react(s):
    i = 0
    txt = s
    while i < len(txt) - 1:
        i += 1
        if txt[i - 1].lower() != txt[i].lower():
            continue
        if txt[i - 1].islower() and txt[i].islower():
            continue
        if txt[i - 1].isupper() and txt[i].isupper():
            continue
        txt = txt[:i - 1] + txt[i + 1:]
        i = max(i - 2, 0)
    return len(txt)


def optimize(s):
    chars = set(s.lower())
    minimum = len(s)
    for char in chars:
        txt = s.replace(char, '').replace(char.upper(), '')
        result = react(txt)
        if result < minimum:
            minimum = result
    return minimum


class TestDay(unittest.TestCase):
    def test_react(self):
        self.assertEqual(react("dabAcCaCBAcCcaDA"), 10)

    def test_optimize(self):
        self.assertEqual(optimize("dabAcCaCBAcCcaDA"), 4)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day05.txt")
    print("Part 1: ", react(txt))
    print("Part 2: ", optimize(txt))
