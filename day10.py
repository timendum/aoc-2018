import re
from statistics import variance

from utils import load_string, run_testCase, unittest

TEST = """position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>"""

OUTTEST = """#...#..###
#...#...#.
#...#...#.
#####...#.
#...#...#.
#...#...#.
#...#...#.
#...#..###"""

RE_NUMBERS = re.compile(r'-?\d+')


def plane_to_string(plane):
    xmax = max([p[0] for p in plane])
    ymax = max([p[1] for p in plane])
    xmin = min([p[0] for p in plane])
    ymin = min([p[1] for p in plane])
    printed = [['.'] * (xmax - xmin + 1) for j in range(ymax - ymin + 1)]
    for point in plane:
        printed[point[1] - ymin][point[0] - xmin] = '#'
    return '\n'.join([''.join(row) for row in printed])


def fast_forward_print(s, nloops=10312):
    lines = s.splitlines()
    plane = []
    velocities = []
    for line in lines:
        matches = RE_NUMBERS.findall(line)
        plane.append([int(matches[0]), int(matches[1])])
        velocities.append((int(matches[2]), int(matches[3])))
    velocities = tuple(velocities)
    i = 0
    deviations = []
    while i != nloops:
        i += 1
        for n, _ in enumerate(plane):
            plane[n][0] += velocities[n][0]
            plane[n][1] += velocities[n][1]
        deviations.append((i, variance([p[0] for p in plane]), variance([p[1] for p in plane])))
    deviations.sort(key=lambda p: p[1] + p[0])
    return plane_to_string(plane)


def fast_forward_num(s, nloops=20000):
    lines = s.splitlines()
    plane = []
    velocities = []
    for line in lines:
        matches = RE_NUMBERS.findall(line)
        plane.append([int(matches[0]), int(matches[1])])
        velocities.append((int(matches[2]), int(matches[3])))
    velocities = tuple(velocities)
    i = 0
    deviations = []
    while i != nloops:
        i += 1
        for n, _ in enumerate(plane):
            plane[n][0] += velocities[n][0]
            plane[n][1] += velocities[n][1]
        deviations.append((i, variance([p[0] for p in plane]), variance([p[1] for p in plane])))
    deviations.sort(key=lambda p: p[1] + p[0])
    return deviations[0][0]


class TestDay(unittest.TestCase):
    def test_fast_forward(self):
        self.assertEqual(fast_forward_print(TEST, 3), OUTTEST)

    def test_order_steps(self):
        self.assertEqual(fast_forward_num(TEST, 5), 3)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day10.txt")
    print("Part 1: \n", fast_forward_print(txt))
    print("Part 2: ", fast_forward_num(txt))
