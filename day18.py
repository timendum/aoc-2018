from collections import Counter
from typing import Dict

from utils import load_string, run_testCase, unittest

TEST = """.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|."""

OPEN = '.'
TREE = '|'
LUMB = '#'

ADIR = (+1, +1 - 1j, -1j, -1 - 1j, -1, -1 + 1j, +1j, +1 + 1j)


def init_board(s: str) -> Dict[complex, str]:
    lines = s.splitlines()
    board = {}
    for y, line in enumerate(lines):
        for x, c in enumerate(line):
            board[x + y * 1j] = c
    return board


def one_minute(oboard: Dict[complex, str]) -> Dict[complex, str]:
    board = {}
    for k in oboard:
        adjacents = Counter([oboard.get(k + p, None) for p in ADIR])
        if oboard[k] == OPEN:
            board[k] = TREE if adjacents[TREE] >= 3 else OPEN
        elif oboard[k] == TREE:
            board[k] = LUMB if adjacents[LUMB] >= 3 else TREE
        else:
            board[k] = LUMB if (adjacents[LUMB] >= 1 and adjacents[TREE] >= 1) else OPEN
    return board


def printb(board: Dict[complex, str]) -> None:
    xmax = max([int(k.real) for k in board])
    ymax = max([int(k.imag) for k in board])
    for y in range(0, ymax + 1):
        for x in range(0, xmax + 1):
            print(board[x + y * 1j], end='')
        print('')
    print('')


def resource_value(s: str) -> int:
    board = init_board(s)
    # printb(board)
    for _ in range(0, 10):
        board = one_minute(board)
        # printb(board)
    wooded = sum([1 for v in board.values() if v == TREE])
    lumberyards = sum([1 for v in board.values() if v == LUMB])
    return wooded * lumberyards


def resource_value_many(s: str) -> int:
    board = init_board(s)
    seen = [board]
    for _ in range(0, 1000000000):
        board = one_minute(board)
        if board in seen:
            seen.append(board)
            break
        seen.append(board)
    last = len(seen) - 1
    prev = seen.index(seen[-1])
    nloop = last - prev
    final = (1000000000 - prev) % nloop
    board = seen[prev + final]
    wooded = sum([1 for v in board.values() if v == TREE])
    lumberyards = sum([1 for v in board.values() if v == LUMB])
    return wooded * lumberyards


class TestDay(unittest.TestCase):
    def test_resource_value(self):
        self.assertEqual(resource_value(TEST), 1147)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day18.txt")
    print("Part 1: ", resource_value(txt))
    print("Part 2: ", resource_value_many(txt))
