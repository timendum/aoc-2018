from utils import load_string, run_testCase, unittest

TEST = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""


def order_steps(s):
    lines = s.splitlines()
    parents = set()
    for line in lines:
        instr = line.split(' ')
        parents.add((instr[1], instr[7]))
    todo = set([parent[0] for parent in parents] + [parent[1] for parent in parents])
    steps = []
    while todo:
        goods = todo - frozenset([parent[1] for parent in parents])
        goods = sorted(goods)
        step = goods[0]
        steps.append(step)
        todo.remove(step)
        for item in list(parents):
            if item[0] == step:
                parents.remove(item)
    return ''.join(steps)


def time_steps(s, nworkres, worktime):
    lines = s.splitlines()
    parents = set()
    for line in lines:
        instr = line.split(' ')
        parents.add((instr[1], instr[7]))
    todo = set([parent[0] for parent in parents] + [parent[1] for parent in parents])
    seconds = -1
    tasks = [None] * nworkres
    ends = [0] * nworkres
    while todo:
        seconds = max(seconds + 1, min(ends))
        for i in range(nworkres):
            if ends[i] == seconds:
                step = tasks[i]
                tasks[i] = None
                if step:
                    todo.remove(step)
                    for item in list(parents):
                        if item[0] == step:
                            parents.remove(item)
        goods = todo - frozenset([parent[1] for parent in parents]) - frozenset(tasks)
        goods = sorted(goods)
        for i in range(nworkres):
            if not goods:
                break
            if not tasks[i]:
                tasks[i] = goods.pop(0)
                ends[i] = seconds + worktime + ord(tasks[i]) - ord('A') + 1
    return seconds


class TestDay(unittest.TestCase):
    def test_order_steps(self):
        self.assertEqual(order_steps(TEST), "CABDFE")

    def test_time_steps(self):
        self.assertEqual(time_steps(TEST, 2, 0), 15)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day07.txt")
    print("Part 1: ", order_steps(txt))
    print("Part 2: ", time_steps(txt, 5, 60))
