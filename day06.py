from math import inf

from utils import load_string, run_testCase, unittest

TEST = """1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"""


def find_inner_values(points):
    inner = []
    for (x, y), value in points.items():
        ne = se = no = so = None
        for a, b in points:
            if a > x and b > y:
                se = (a, b)
            elif a > x and b < y:
                ne = (a, b)
            elif a < x and b > y:
                no = (a, b)
            elif a < x and b < y:
                so = (a, b)
        if ne is not None and se is not None and no is not None and so is not None:
            inner.append(value)
    return tuple(inner)


def find_limits(points):
    return (min([point[0] for point in points]), min([point[1] for point in points]),
            max([point[0] for point in points]), max([point[1] for point in points]))


def find_nearest(x, y, points):
    nearests = None
    ldistance = inf
    for px, py in points:
        distance = abs(x - px) + abs(y - py)
        if distance < ldistance:
            nearests = [(px, py)]
            ldistance = distance
        elif distance == ldistance:
            nearests.append((px, py))
    if len(nearests) == 1:
        return points[nearests[0]]
    return None


def larger_size(s):
    lines = s.splitlines()
    points = {}
    for i, line in enumerate(lines):
        x, y = line.split(', ')
        points[(int(x), int(y))] = i
    xmin, ymin, xmax, ymax = find_limits(points)
    inner_values = find_inner_values(points)
    sizes = {values: 0 for values in inner_values}
    for x in range(xmin, xmax + 1):
        for y in range(ymin, ymax + 1):
            nearest = find_nearest(x, y, points)
            if nearest not in inner_values:
                continue
            sizes[nearest] += 1
    return max(sizes.values())


def find_safe(s, limit):
    lines = s.splitlines()
    points = {}
    for i, line in enumerate(lines):
        x, y = line.split(', ')
        points[(int(x), int(y))] = i
    xmin, ymin, xmax, ymax = find_limits(points)
    safe_points = []
    for x in range(xmin, xmax + 1):
        for y in range(ymin, ymax + 1):
            distance = 0
            for (px, py) in points:
                distance += abs(x - px) + abs(y - py)
            if distance < limit:
                safe_points.append((x, y))
    return len(safe_points)


class TestDay(unittest.TestCase):
    def test_react(self):
        self.assertEqual(larger_size(TEST), 17)

    def test_find_safe(self):
        self.assertEqual(find_safe(TEST, 32), 16)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day06.txt")
    print("Part 1: ", larger_size(txt))
    print("Part 2: ", find_safe(txt, 10000))
