import re
from typing import List

from utils import load_string, run_testCase, unittest

TEST = """Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]"""

OPS = {
    'addr': lambda reg, a, b: reg[a] + reg[b],
    'addi': lambda reg, a, b: reg[a] + b,
    'mulr': lambda reg, a, b: reg[a] * reg[b],
    'muli': lambda reg, a, b: reg[a] * b,
    'banr': lambda reg, a, b: reg[a] & reg[b],
    'bani': lambda reg, a, b: reg[a] & b,
    'borr': lambda reg, a, b: reg[a] | reg[b],
    'bori': lambda reg, a, b: reg[a] | b,
    'setr': lambda reg, a, b: reg[a],
    'seti': lambda reg, a, b: a,
    'gtir': lambda reg, a, b: 1 if a > reg[b] else 0,
    'gtri': lambda reg, a, b: 1 if reg[a] > b else 0,
    'gtrr': lambda reg, a, b: 1 if reg[a] > reg[b] else 0,
    'eqir': lambda reg, a, b: 1 if a == reg[b] else 0,
    'eqri': lambda reg, a, b: 1 if reg[a] == b else 0,
    'eqrr': lambda reg, a, b: 1 if reg[a] == reg[b] else 0,
}


def process_opcode(opname: str, before: List[int], opinput: List[int]) -> List[int]:
    res = OPS[opname](before, opinput[1], opinput[2])
    after = before.copy()
    after[opinput[3]] = res
    return after


def three_more(s: str) -> int:
    inputs = s.split('\n\n\n')[0].split('\n\n')
    res = 0
    for inpt in inputs:
        lines = inpt.splitlines()
        before = [int(n) for n in re.findall(r'\d+', lines[0])]
        opinput = [int(n) for n in re.findall(r'\d+', lines[1])]
        after = [int(n) for n in re.findall(r'\d+', lines[2])]
        candidates = 0
        for k in OPS:
            rafter = process_opcode(k, before, opinput)
            if rafter == after:
                candidates += 1
        if candidates >= 3:
            res += 1
    return res


def find_opsnumbers(s: str) -> List[str]:
    inputs = s.split('\n\n\n')[0].split('\n\n')
    candidates = [set(OPS.keys()) for k in range(0, len(OPS))]
    for inpt in inputs:
        lines = inpt.splitlines()
        before = [int(n) for n in re.findall(r'\d+', lines[0])]
        opinput = [int(n) for n in re.findall(r'\d+', lines[1])]
        after = [int(n) for n in re.findall(r'\d+', lines[2])]
        for k in OPS:
            if k not in candidates[opinput[0]]:
                continue
            rafter = process_opcode(k, before, opinput)
            if rafter != after:
                candidates[opinput[0]].discard(k)
    opcodes = [''] * len(OPS)
    while min([len(a) for a in opcodes]) < 1:
        for i, v in enumerate(candidates):
            if len(v) == 1:
                opcodes[i] = v.pop()
                for ca in candidates:
                    ca.discard(opcodes[i])
    return opcodes


def workout(s: str) -> int:
    opcodes = find_opsnumbers(s)
    lines = s.split('\n\n\n')[1].split('\n')
    regs = [0] * 4
    for line in lines:
        if not line:
            continue
        opinput = [int(n) for n in line.split(' ')]
        regs = process_opcode(opcodes[opinput[0]], regs, opinput)
    return regs[0]


class TestDay(unittest.TestCase):
    def atest_number_plant(self):
        self.assertEqual(three_more(TEST), 1)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day16.txt")
    print("Part 1: ", three_more(txt))
    print("Part 2: ", workout(txt))
