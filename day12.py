from collections import defaultdict

from utils import load_string, run_testCase, unittest

TEST = """initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"""


def init_state(s):
    line = s.splitlines()[0]
    line = line.split(' ')[2]
    state = defaultdict(bool)
    for i, c in enumerate(line):
        state[i] = (c == '#')
    return state


def init_spreads(s):
    def spread(line):
        return lambda i, state: all([state[i - 2 + d] == (line[d] == '#') for d in range(5)])

    spreads = []
    lines = s.splitlines()
    for line in lines[2:]:
        if line[9] != '#':
            continue
        spreads.append(spread(line))
    return spreads


def generate_spreads_lambda(s, state):
    #print(''.join(['#' if state[k] else '.' for k in range(-5, 100)]))
    spreads = init_spreads(s)
    new_state = defaultdict(bool)
    for i in range(min(state) - 2, max(state) + 2):
        for spread in spreads:
            new_state[i] = spread(i, state)
            if new_state[i]:
                break
        if not new_state[i]:
            del new_state[i]
    return new_state


def generate_spreads_text(s, state):
    lines = s.splitlines()
    #print(''.join(['#' if state[k] else '.' for k in range(-5, 100)]))
    new_state = defaultdict(bool)
    for i in range(min(state) - 2, max(state) + 2):
        for line in lines[2:]:
            if line[9] != '#':
                continue
            new_state[i] = all([state[i - 2 + d] == (line[d] == '#') for d in range(5)])
            if new_state[i]:
                break
        if not new_state[i]:
            del new_state[i]
    return new_state


def number_many_plant(s):
    state = init_state(s)
    sums = [0] * 401
    for g in range(401):
        state = generate_spreads_text(s, state)
        sums[g] = sum([k for k, v in state.items() if v])
    return sums[299] + (sums[400] - sums[300]) * ((50000000000 - 300) // 100)


def number_plant(s):
    state = init_state(s)
    for _ in range(20):
        state = generate_spreads_text(s, state)
    return sum([k for k, v in state.items() if v])


class TestDay(unittest.TestCase):
    def test_number_plant(self):
        self.assertEqual(number_plant(TEST), 325)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day12.txt")
    print("Part 1: ", number_plant(txt))
    print("Part 2: ", number_many_plant(txt))
