import difflib
import unittest
from collections import Counter

from utils import load_string, run_testCase

TEST_1 = """abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab"""

TEST_2 = """abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz"""


def count_checksum(s):
    lines = s.splitlines()
    twos = 0
    threes = 0
    for line in lines:
        counts = Counter(line)
        for _, v in counts.most_common():
            if v == 2:
                twos += 1
                break
        for _, v in counts.most_common():
            if v == 3:
                threes += 1
                break
    return twos * threes


def common_ids(s):
    lines = s.splitlines()
    for bline in lines:
        for cline in lines:
            if bline == cline:
                continue
            diff = difflib.SequenceMatcher(None, cline, bline, autojunk=False)
            match_size = sum([mbloc.size for mbloc in diff.get_matching_blocks()])
            if match_size == len(bline) - 1:
                return ''.join(
                    [bline[mbloc.a:mbloc.a + mbloc.size] for mbloc in diff.get_matching_blocks()])


class TestDay(unittest.TestCase):
    def test_count_checksum(self):
        self.assertEqual(count_checksum(TEST_1), 12)

    def test_common_ids(self):
        self.assertEqual(common_ids(TEST_2), "fgij")


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day02.txt")
    print("Part 1: ", count_checksum(txt))
    print("Part 2: ", common_ids(txt))
