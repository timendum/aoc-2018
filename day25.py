import re
from typing import List, Tuple

from utils import load_string, run_testCase, unittest

TEST_1 = """0,0,0,0
3,0,0,0
0,3,0,0
0,0,3,0
0,0,0,3
0,0,0,6
9,0,0,0
12,0,0,0"""
TEST_2 = """-1,2,2,0
0,0,2,-2
0,0,0,-2
-1,2,0,0
-2,-2,-2,2
3,0,2,-1
-1,3,2,2
-1,0,-1,0
0,2,1,-2
3,0,0,0"""
TEST_3 = """1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2"""
TEST_4 = """1,-1,-1,-2
-2,-2,0,1
0,2,1,3
-2,3,-2,1
0,2,3,-2
-1,-1,1,-2
0,-2,-1,0
-2,2,3,-1
1,2,2,0
-1,-2,0,-2"""


def fits(point: Tuple[int], costellation: List[Tuple[int]]) -> bool:
    for p in costellation:
        if sum([abs(x - y) for x, y in zip(p, point)]) <= 3:
            return True
    return False


def merge(costellations: List[List[Tuple[int]]]):
    for i, a in enumerate(costellations):
        for b in costellations[i + 1:]:
            for point in b:
                if fits(point, a):
                    print('merge', a, b, len(costellations))
                    b.extend(a)
                    costellations = costellations[0:i] + costellations[i + 1:]
                    return costellations, True
    return costellations, False


def count_constellations(s: str) -> int:
    constels = []  # type: List[int]
    lines = s.splitlines()
    points = []  # type: List[Tuple[int, ...]]
    for line in lines:
        point = tuple([int(n) for n in re.findall(r'-?\d+', line)])  # type: Tuple[int, ...]
        points.append(point)
        constels.append(len(points))
    for i, point in enumerate(points):
        for j, p in enumerate(points[i:], i):
            if constels[i] == constels[j]:
                continue
            if sum([abs(x - y) for x, y in zip(p, point)]) <= 3:
                constels = [constels[i] if c == constels[j] else c for c in constels]
    return len(set(constels))


class TestDay(unittest.TestCase):
    def test_count_constellations(self):
        self.assertEqual(count_constellations(TEST_1), 2)
        self.assertEqual(count_constellations(TEST_2), 4)
        self.assertEqual(count_constellations(TEST_3), 3)
        self.assertEqual(count_constellations(TEST_4), 8)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day25.txt")
    print("Part 1: ", count_constellations(txt))
