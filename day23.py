import re
from operator import itemgetter
from typing import List, Tuple

from utils import load_string, run_testCase, unittest

TEST_1 = """pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1"""

TEST_2 = """pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5"""


def in_range(s: str) -> int:
    lines = s.splitlines()
    nanos = []  # type: List[Tuple[int, ...]]
    for line in lines:
        nano = tuple([int(n) for n in re.findall(r'-?\d+', line)])  # type: Tuple[int, ...]
        nanos.append(nano)
    mnano = sorted(nanos, key=itemgetter(3))[-1]
    res = 0
    for nano in nanos:
        if sum([abs(x - y) for x, y in zip(mnano[:3], nano[:3])]) <= mnano[3]:
            res += 1
    return res


class TestDay(unittest.TestCase):
    def test_in_range(self):
        self.assertEqual(in_range(TEST_1), 7)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day23.txt")
    print("Part 1: ", in_range(txt))
