from collections import defaultdict
from typing import Dict

from utils import load_string, run_testCase, unittest

TEST_1 = "^ENWWW(NEEE|SSE(EE|N))$"
TEST_2 = "^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$"
TEST_3 = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$"
TEST_4 = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"

START = 'X'
OPEN = '.'
DOOR_V = '-'
DOOR_H = '|'
WALL = '#'
TBD = '?'

DIRS = {'N': -1j, 'S': +1j, 'E': +1, 'W': -1}

ALL_DIRS = DIRS.values()


def fill_open(board: Dict[complex, str], pos: complex) -> None:
    board[pos] = OPEN
    for d in ALL_DIRS:
        board[pos + d] = board.get(pos + d, TBD)


def fill_wall(board: Dict[complex, str], pos: complex, d: complex) -> None:
    if d in (+1, -1):
        board[pos - 1j] = WALL
        board[pos + 1j] = WALL
        board[pos] = DOOR_V
    else:
        board[pos - 1] = WALL
        board[pos + 1] = WALL
        board[pos] = DOOR_H


def build_map(s: str) -> Dict[complex, int]:
    pos = 0j
    board = {}  # type:  Dict[complex, str]
    fill_open(board, pos)
    board[pos] = START
    savepoints = [pos]
    distances = defaultdict(int)  # type:  Dict[complex, int]
    cdist = 0
    distances[pos] = cdist
    for c in tuple(s):
        if c in DIRS:
            pos += DIRS[c]
            fill_wall(board, pos, DIRS[c])
            pos += DIRS[c]
            fill_open(board, pos)
            cdist += 1
            if distances[pos]:
                distances[pos] = min(distances[pos], cdist)
                cdist = distances[pos]
            else:
                distances[pos] = cdist
        elif c == '(':
            savepoints.append(pos)
        elif c == '|':
            pos = savepoints[-1]
            cdist = distances[pos]
        elif c == ')':
            pos = savepoints.pop()
    for k, v in board.items():
        if v == TBD:
            board[k] = WALL
    # print_board(board, distances)
    return distances


def largest(s: str) -> int:
    distances = build_map(s)
    return max(distances.values())


def howmany(s: str) -> int:
    distances = build_map(s)
    return len([k for k, v in distances.items() if v >= 1000])


def print_board(board: Dict[complex, str], distances: Dict[complex, int]) -> None:
    xmin = min([int(x.real) for x in board])
    xmax = max([int(x.real) for x in board])
    ymin = min([int(x.imag) for x in board])
    ymax = max([int(x.imag) for x in board])
    print('')
    for y in range(ymin, ymax + 1):
        for x in range(xmin, xmax + 1):
            c = board.get(x + y * 1j, WALL)
            if c == OPEN:
                c = str(distances[x + y * 1j] % 10)
            print(c, end='')
        print('')


class TestDay(unittest.TestCase):
    def test_resource_value(self):
        self.assertEqual(largest(TEST_1), 10)
        self.assertEqual(largest(TEST_2), 18)
        self.assertEqual(largest(TEST_3), 23)
        self.assertEqual(largest(TEST_4), 31)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day20.txt")
    print("Part 1: ", largest(txt))
    print("Part 2: ", howmany(txt))
