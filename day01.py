from itertools import cycle
import re
import unittest

from utils import load_string, run_testCase


def sum_signed(s):
    return sum(int(number) for number in re.split(r'\s', s))


def list_signed(s):
    r = 0
    numbers = re.split(r'\s', s)
    seen = set([0])
    for n in cycle(numbers):
        r += int(n)
        if r in seen:
            return r
        seen.add(r)
    return None


class TestDay(unittest.TestCase):
    def test_sum_signed(self):
        self.assertEqual(sum_signed("+1 +1 +1"), 3)
        self.assertEqual(sum_signed("+1 +1 -2"), 0)
        self.assertEqual(sum_signed("-1 -2 -3"), -6)

    def test_list_signed(self):
        self.assertEqual(list_signed("+1 -1"), 0)
        self.assertEqual(list_signed("+3 +3 +4 -2 -4"), 10)
        self.assertEqual(list_signed("-6 +3 +8 +5 -6"), 5)
        self.assertEqual(list_signed("+7 +7 -2 -7 -4"), 14)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day01.txt")
    print("Part 1: ", sum_signed(txt))
    print("Part 2: ", list_signed(txt))
