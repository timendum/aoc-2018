from collections import defaultdict
import re
from typing import Dict, Tuple, Optional

from utils import load_string, run_testCase, unittest

TEST = """x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504"""

SAND = 0
CLAY = 1
WATER = 2
FLOW = -1

CPRINT = {SAND: '.', CLAY: '#', WATER: '~', FLOW: '|'}

RE_NUMS = re.compile(r'(\d+)')

OUTFILE = open('out17.txt', 'wt')


class Board():
    def __init__(self, s: str) -> None:
        lines = s.splitlines()
        self.board = defaultdict(int)  # type: Dict[Tuple[int, int], int]
        xs = set([500])
        ys = set([0])
        for line in lines:
            if line[0] == 'x':
                numbers = RE_NUMS.findall(line)
                numbers = [int(n) for n in numbers]
                xs.add(numbers[0])
                for y in range(numbers[1], numbers[2] + 1):
                    self.board[(numbers[0], y)] = 1
                    ys.add(y)
            else:
                numbers = RE_NUMS.findall(line)
                numbers = [int(n) for n in numbers]
                ys.add(numbers[0])
                for x in range(numbers[1], numbers[2] + 1):
                    self.board[(x, numbers[0])] = 1
                    xs.add(x)
        sxs = sorted(xs)
        self.xmin, self.xmax = sxs[0] - 1, sxs[-1] + 2
        sys = sorted(ys)
        self.ymin, self.ymax = sys[0], sys[-1] + 2

    def fall_down(self, ox, oy):
        wx = ox
        for wy in range(oy, self.ymax + 1):
            if self.board[wx, wy + 1] != CLAY:
                self.board[wx, wy + 1] = FLOW
            else:
                return (wx, wy)
        return None

    def spread(self, ox, oy):
        if self.board[(ox, oy)] == WATER:
            return []
        newf = [None, None]
        wx = ox
        wy = oy
        row = set()
        # to the left [0]
        while self.board[(wx, wy)] != CLAY:
            row.add((wx, wy))
            if self.board[(wx, wy + 1)] not in (CLAY, WATER):
                newf[0] = wx
                break
            wx -= 1
        wx = ox
        while self.board[(wx, wy)] != CLAY:
            row.add((wx, wy))
            if self.board[(wx, wy + 1)] not in (CLAY, WATER):
                newf[1] = wx
                break
            wx += 1
        if any(newf):
            # unbounded
            for cell in row:
                self.board[cell] = FLOW
        else:
            # bounded
            for cell in row:
                self.board[cell] = WATER
            return self.spread(ox, oy - 1)
        return [(x, oy) for x in newf if x]

    def print(self):
        print(' ', end='', file=OUTFILE, flush=True)
        for x in range(self.xmin, self.xmax):
            print(x % 10, end='', file=OUTFILE)
        print('', file=OUTFILE)
        for y in range(self.ymin, self.ymax):
            print(y % 10, end='', file=OUTFILE)
            for x in range(self.xmin, self.xmax):
                if y == 0 and x == 500:
                    print('+', end='', file=OUTFILE)
                    continue
                print(CPRINT[self.board[x, y]], end='', file=OUTFILE)
            print('', file=OUTFILE)

    def count_water(self):
        return len(
            [v for k, v in self.board.items() if v in (WATER, FLOW) and k[1] < self.ymax - 1])


def water_reach(s: str) -> int:
    board = Board(s)
    flows = [(500, 0)]
    spreads = flows
    spreads = []
    board.print()
    while spreads or flows:
        while flows:
            new_spreads = board.fall_down(*flows.pop())
            if new_spreads:
                spreads.append(new_spreads)
        board.print()
        while spreads:
            new_flows = board.spread(*spreads.pop())
            if new_flows:
                flows.extend(new_flows)
        # board.print()
    # board.print()
    return board.count_water()


class TestDay(unittest.TestCase):
    def test_water_reach(self):
        self.assertEqual(water_reach(TEST), 57)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day17.txt")
    print("Part 1: ", water_reach(txt))
