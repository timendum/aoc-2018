import re
import unittest
from collections import defaultdict

from utils import load_string, run_testCase

TEST = """#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2"""

RE_LINE = re.compile(r'\#(?P<id>\d+) @ (?P<sx>\d+),(?P<sy>\d+): (?P<lx>\d+)x(?P<ly>\d+)')


def count_overlaps(s):
    suit = defaultdict(set)
    lines = s.splitlines()
    for line in lines:
        patch = RE_LINE.match(line).groupdict()
        patch = {
            'id': patch['id'],
            'sx': int(patch['sx']),
            'sy': int(patch['sy']),
            'lx': int(patch['lx']),
            'ly': int(patch['ly']),
        }
        for x in range(patch['sx'], patch['sx'] + patch['lx']):
            for y in range(patch['sy'], patch['sy'] + patch['ly']):
                suit[(x, y)].add(patch['id'])
    return len([place for place in suit.values() if len(place) > 1])


def no_overlap(s):
    suit = defaultdict(set)
    lines = s.splitlines()
    ids = set()
    for line in lines:
        patch = RE_LINE.match(line).groupdict()
        patch = {
            'id': patch['id'],
            'sx': int(patch['sx']),
            'sy': int(patch['sy']),
            'lx': int(patch['lx']),
            'ly': int(patch['ly']),
        }
        ids.add(patch['id'])
        for x in range(patch['sx'], patch['sx'] + patch['lx']):
            for y in range(patch['sy'], patch['sy'] + patch['ly']):
                suit[(x, y)].add(patch['id'])
    for place in suit.values():
        if len(place) > 1:
            ids = ids.difference(place)
    return next(iter(ids))


class TestDay(unittest.TestCase):
    def test_count_overlaps(self):
        self.assertEqual(count_overlaps(TEST), 4)

    def test_no_overlap(self):
        self.assertEqual(no_overlap(TEST), '3')


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day03.txt")
    print("Part 1: ", count_overlaps(txt))
    print("Part 2: ", no_overlap(txt))
