from itertools import cycle
from typing import List

from utils import load_string, run_testCase, unittest

TEST_1 = r"""/->-\        
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/   """

TEST_2 = r"""/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/"""

SDIRECTIONS = {
    '>': 1,
    '<': -1,
    '^': -1j,
    'v': 1j,
}

CDIRECTIONS = {v: k for k, v in SDIRECTIONS.items()}


class Cart():
    def __init__(self, pos: complex, c: str) -> None:
        self.pos = pos
        self.dir = SDIRECTIONS[c]
        self.crashed = False
        self._cross_handler = cycle((-1j, 1, 1j))

    def move(self):
        if self.crashed:
            return
        self.pos = self.pos + self.dir

    def to_char(self) -> str:
        if self.crashed:
            return 'X'
        return CDIRECTIONS[self.dir]

    def turn(self, cell: str) -> None:
        if cell == '+':
            self.dir = self.dir * next(self._cross_handler)
        elif cell == '\\':
            if self.dir.real != 0:
                self.dir = self.dir * 1j
            else:
                self.dir = self.dir * -1j
        elif cell == '/':
            if self.dir.real != 0:
                self.dir = self.dir * -1j
            else:
                self.dir = self.dir * 1j

    def __repr__(self):
        return 'Cart({:d}, {:d}, {})'.format(int(self.pos.real), int(self.pos.imag), self.to_char())


def init_board(s: str) -> List[List[str]]:
    lines = s.splitlines()
    board = [[' ' for _ in enumerate(lines)] for _ in enumerate(lines[1])]
    for y, line in enumerate(lines):
        for x, c in enumerate(line):
            if c in ('v', '^'):
                board[x][y] = '|'
            elif c in ('>', '<'):
                board[x][y] = '-'
            elif c in '+-|\\/':
                board[x][y] = c
    return board


def init_carts(s: str) -> List[Cart]:
    lines = s.splitlines()
    carts = []  # type: List[Cart]
    for y, line in enumerate(lines):
        for x, c in enumerate(line):
            if c in ('v', '^', '>', '<'):
                carts.append(Cart(x + y * 1j, c))
    return carts


def print_state(board: List[List[str]], carts: List[Cart]):
    print(' ', end='')
    for x, _ in enumerate(board):
        print(x % 10, end='')
    print(' ')
    for y, _ in enumerate(board[0]):
        print(y % 10, end='')
        for x, _ in enumerate(board):
            for cart in carts:
                if cart.pos == x + y * 1j:
                    print(cart.to_char(), end='')
                    break
            else:
                print(board[x][y], end='')
        print(' ')


def find_first_crash(s: str):
    board = init_board(s)
    carts = init_carts(s)
    while not any([cart for cart in carts if cart.crashed]):
        carts = sorted(carts, key=lambda cart: (cart.pos.imag, cart.pos.real))
        # print_state(board, carts)
        for cart in carts:
            cart.move()
            cart.turn(board[int(cart.pos.real)][int(cart.pos.imag)])
            for bcart in carts:
                if bcart != cart and cart.pos == bcart.pos:
                    cart.crashed = True
                    bcart.crashed = True
    crashed = [cart for cart in carts if cart.crashed][0]
    return '{:d},{:d}'.format(int(crashed.pos.real), int(crashed.pos.imag))


def find_last_cart(s: str):
    board = init_board(s)
    carts = init_carts(s)
    while len(carts) > 1:
        carts = sorted(carts, key=lambda cart: (cart.pos.imag, cart.pos.real))
        # print_state(board, carts)
        for cart in carts:
            cart.move()
            cart.turn(board[int(cart.pos.real)][int(cart.pos.imag)])
            for ocart in carts:
                if ocart != cart and cart.pos == ocart.pos:
                    cart.crashed = True
                    ocart.crashed = True
        carts = [cart for cart in carts if not cart.crashed]
    return '{:d},{:d}'.format(int(carts[0].pos.real), int(carts[0].pos.imag))


class TestDay(unittest.TestCase):
    def test_number_plant(self):
        self.assertEqual(find_first_crash(TEST_1), '7,3')

    def test_find_last_cart(self):
        self.assertEqual(find_last_cart(TEST_2), '6,4')


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day13.txt")
    print("Part 1:", find_first_crash(txt))
    print("Part 2:", find_last_cart(txt))
