from utils import load_string, run_testCase, unittest

TEST = """2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"""


def incremental_sum(inputs):
    nchild = inputs.pop(0)
    nmetadata = inputs.pop(0)
    r = 0
    for _ in range(nchild):
        r += incremental_sum(inputs)
    for _ in range(nmetadata):
        r += inputs.pop(0)
    return r


def sum_data(s):
    inputs = [int(d) for d in s.split(' ')]
    return incremental_sum(inputs)


def incremental_value(inputs):
    nchild = inputs.pop(0)
    nmetadata = inputs.pop(0)
    metadatas = []
    child = []
    for _ in range(nchild):
        child.append(incremental_value(inputs))
    for _ in range(nmetadata):
        metadatas.append(inputs.pop(0))
    if nchild == 0:
        return sum(metadatas)
    r = 0
    for meta in metadatas:
        try:
            r += child[meta - 1]
        except IndexError:
            pass
    return r


def value_sum(s):
    inputs = [int(d) for d in s.split(' ')]
    return incremental_value(inputs)


class TestDay(unittest.TestCase):
    def test_order_steps(self):
        self.assertEqual(sum_data(TEST), 138)

    def test_value_sum(self):
        self.assertEqual(value_sum(TEST), 66)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day08.txt")
    print("Part 1: ", sum_data(txt))
    print("Part 2: ", value_sum(txt))
