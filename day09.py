from collections import defaultdict, deque

from utils import load_string, run_testCase, unittest


def highest_score(s):
    players, nmarble = int(s.split(' ')[0]), int(s.split(' ')[6])
    circle = deque([0])
    scores = defaultdict(int)
    player = 0
    for marble in range(1, nmarble + 1):
        if marble % 23 != 0:
            circle.rotate(-1)
            circle.append(marble)
        else:
            circle.rotate(7)
            scores[player] += marble + circle.pop()
            circle.rotate(-1)
        player = (player + 1) % players
    return max(scores.values())


def highest_score_larger(s):
    players, nmarble = int(s.split(' ')[0]), int(s.split(' ')[6])
    circle = deque([0])
    scores = defaultdict(int)
    nmarble = nmarble * 100
    player = 0
    for marble in range(1, nmarble + 1):
        if marble % 23 != 0:
            circle.rotate(-1)
            circle.append(marble)
        else:
            circle.rotate(7)
            scores[player] += marble + circle.pop()
            circle.rotate(-1)
        player = (player + 1) % players
    return max(scores.values())


class TestDay(unittest.TestCase):
    def test_order_steps(self):
        self.assertEqual(highest_score("5 players; last marble is worth 25 points"), 32)
        self.assertEqual(highest_score("10 players; last marble is worth 1618 points"), 8317)
        self.assertEqual(highest_score("13 players; last marble is worth 7999 points"), 146373)
        self.assertEqual(highest_score("17 players; last marble is worth 1104 points"), 2764)
        self.assertEqual(highest_score("21 players; last marble is worth 6111 points"), 54718)
        self.assertEqual(highest_score("30 players; last marble is worth 5807 points"), 37305)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day09.txt")
    print("Part 1: ", highest_score(txt))
    print("Part 2: ", highest_score_larger(txt))
