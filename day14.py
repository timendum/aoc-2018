from utils import load_string, run_testCase, unittest


def print_scores(recipes, elfa, elfb):
    precipes = [None] * len(recipes)
    for i, recipe in enumerate(recipes):
        if i == elfa:
            precipes[i] = '({:d})'.format(recipe)
        elif i == elfb:
            precipes[i] = '[{:d}]'.format(recipe)
        else:
            precipes[i] = ' {:d} '.format(recipe)
    print(''.join(precipes))


def scores_after(s):
    n = int(s)
    recipes = [3, 7]
    elfa, elfb = 0, 1

    # print_scores(recipes, elfa, elfb)
    for _ in range(n + 8):
        new_recipe = recipes[elfa] + recipes[elfb]
        if new_recipe >= 10:
            recipes.extend([new_recipe // 10, new_recipe % 10])
        else:
            recipes.append(new_recipe)
        elfa = (elfa + recipes[elfa] + 1) % len(recipes)
        elfb = (elfb + recipes[elfb] + 1) % len(recipes)
        # print_scores(recipes, elfa, elfb)
    return ''.join([str(r) for r in recipes[n:n + 10]])


def count_left(s):
    recipes = [3, 7]
    elfa, elfb = 0, 1
    target = [int(c) for c in s]

    while target != recipes[-len(target):] and target != recipes[-len(target) - 1:-1]:
        new_recipe = recipes[elfa] + recipes[elfb]
        if new_recipe >= 10:
            recipes.extend([new_recipe // 10, new_recipe % 10])
        else:
            recipes.append(new_recipe)
        elfa = (elfa + recipes[elfa] + 1) % len(recipes)
        elfb = (elfb + recipes[elfb] + 1) % len(recipes)
    return ''.join([str(r) for r in recipes]).index(s)


class TestDay(unittest.TestCase):
    def test_number_plant(self):
        self.assertEqual(scores_after('9'), '5158916779')
        self.assertEqual(scores_after('5'), '0124515891')
        self.assertEqual(scores_after('18'), '9251071085')
        self.assertEqual(scores_after('2018'), '5941429882')

    def test_count_left(self):
        self.assertEqual(count_left('51589'), 9)
        self.assertEqual(count_left('01245'), 5)
        self.assertEqual(count_left('92510'), 18)
        self.assertEqual(count_left('59414'), 2018)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day14.txt")
    print("Part 1: ", scores_after(txt))
    print("Part 1: ", count_left(txt))
