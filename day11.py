from utils import load_string, run_testCase, unittest


def cell_score(x, y, serial):
    rack_id = x + 10
    multiplying = (rack_id * y + serial) * rack_id
    return ((multiplying // 100) % 10) - 5

def largest_power(s):
    serial = int(s)
    board = [[cell_score(x, y, serial) for x in range(1, 301)] for y in range(1, 301)]
    sum_score = {}
    for x in range(300 - 2):
        for y in range(300 - 2):
            sum_score[(x + 1, y + 1)] = sum(map(sum, map(lambda e: e[x:x + 3], board[y:y + 3])))
    return max(sum_score, key=lambda i: sum_score[i])


def largest_square(s):
    serial = int(s)
    board = [[cell_score(x, y, serial) for x in range(1, 301)] for y in range(1, 301)]
    max_score = 0
    score = 0
    max_point = None
    for d in range(1, 20):
        for x in range(300 - d + 1):
            for y in range(300 - d + 1):
                score = sum(map(sum, map(lambda e: e[x:x + d], board[y:y + d])))
                if score > max_score:
                    max_score = score
                    max_point = (x + 1, y + 1, d)
    return max_point


class TestDay(unittest.TestCase):
    def test_cell_score(self):
        self.assertEqual(cell_score(3, 5, 8), 4)
        self.assertEqual(cell_score(122, 79, 57), -5)
        self.assertEqual(cell_score(217, 196, 39), 0)
        self.assertEqual(cell_score(101, 153, 71), 4)

    def test_largest_power(self):
        self.assertEqual(largest_power(18), (33, 45))
        self.assertEqual(largest_power(42), (21, 61))

    def test_largest_square(self):
        self.assertEqual(largest_square(18), (90, 269, 16))
        self.assertEqual(largest_square(42), (232, 251, 12))


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day11.txt")
    print("Part 1: ", largest_power(txt))
    print("Part 2: ", largest_square(txt))
