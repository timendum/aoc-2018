import re
from datetime import datetime, timedelta
import unittest
from collections import OrderedDict, defaultdict

from utils import load_string, run_testCase

RE_LINE = re.compile(r"\[(\d\d\d\d\-\d\d\-\d\d \d\d:\d\d)\] (\w.+)")

TEST = """[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:36] falls asleep
[1518-11-01 00:30] falls asleep
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-05 00:45] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-01 00:25] wakes up
[1518-11-03 00:24] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:55] wakes up"""


def order_logs(lines):
    logs = {}
    for line in lines:
        stime, text = RE_LINE.match(line).groups()
        dtime = datetime.strptime(stime, '%Y-%m-%d %H:%M')
        logs[dtime] = text
    return OrderedDict(sorted(logs.items(), key=lambda t: t[0]))


def create_sleep(logs):
    guard = None
    sleep = defaultdict(list)
    for dtime, text in logs.items():
        if 'begins shift' in text:
            guard = text.split()[1][1:]
        elif 'asleep' in text:
            sleep[guard].append([dtime, None])
        elif 'wakes up' in text:
            sleep[guard][-1][1] = dtime
    return sleep


def create_minutes(sleep_time):
    minutes = defaultdict(int)
    for start_time, end_time in sleep_time:
        current_time = start_time
        while current_time < end_time:
            minutes[current_time.time()] += 1
            current_time = current_time + timedelta(minutes=1)
    return sorted(minutes.items(), key=lambda t: t[1], reverse=True)


def longest_sleep(s):
    lines = s.splitlines()
    logs = order_logs(lines)
    sleep = create_sleep(logs)
    sleep_lenght = {
        guard: sum([((p[1] - p[0]).total_seconds() / 60) for p in sleep[guard]])
        for guard in sleep.keys()
    }
    guard = sorted(sleep_lenght.items(), key=lambda t: t[1], reverse=True)[0][0]
    minutes = create_minutes(sleep[guard])
    return int(guard) * minutes[0][0].minute


def longest_minutes(s):
    lines = s.splitlines()
    logs = order_logs(lines)
    sleep = create_sleep(logs)
    max = [None, None, 0]
    for guard in sleep.keys():
        minutes = create_minutes(sleep[guard])
        if minutes[0][1] > max[2]:
            max = [guard, minutes[0][0].minute, minutes[0][1]]
    return int(max[0]) * max[1]


class TestDay(unittest.TestCase):
    def test_longest_sleep(self):
        self.assertEqual(longest_sleep(TEST), 240)

    def test_longest_minutes(self):
        self.assertEqual(longest_minutes(TEST), 4455)


if __name__ == '__main__':
    run_testCase(TestDay)
    txt = load_string("day04.txt")
    print("Part 1: ", longest_sleep(txt))
    print("Part 2: ", longest_minutes(txt))
